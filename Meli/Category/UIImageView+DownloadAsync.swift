//
//  UIImageView+DownloadAsync.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/29/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

extension UIImageView {
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }

    func downloadImage(url: URL) {
    
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
        
            DispatchQueue.main.async() {
            self.image = UIImage(data: data)
            }
        }
    }
}
