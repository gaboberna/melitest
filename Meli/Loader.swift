//
//  Loader.swift
//  Meli
//
//  Created by Gabriel Bernardini on 11/1/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class Loader: NSObject {
    static let sharedInstance = Loader()
    let activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView();

    func startLoading(view:UIView){
        self.activityIndicator.center = view.center;
        self.activityIndicator.hidesWhenStopped = true;
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray;
       
        view.addSubview(activityIndicator);
        
        activityIndicator.startAnimating();
    }
    
    func stopLoading(){
        self.activityIndicator.stopAnimating();
        self.activityIndicator.removeFromSuperview()
    }
}
