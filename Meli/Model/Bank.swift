//
//  Bank.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/31/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit
import Gloss

class Bank: Glossy {
    var bankId: String!
    var imageName: String!
    var name: String!
    
    required public init?(json: JSON) {
        bankId      = "id"                  <~~ json
        imageName   = "secure_thumbnail"    <~~ json
        name        = "name"                <~~ json
    }
    
    func toJSON() -> JSON? {
        return nil
    }
}
