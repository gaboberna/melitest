//
//  Installment.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/31/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit
import Gloss

class Installment: Glossy {
    var installments: String!
    var message: String!
    
    required public init?(json: JSON) {
        installments    = "installments"        <~~ json
        message         = "recommended_message" <~~ json
    }
    
    func toJSON() -> JSON? {
        return nil
    }
}
