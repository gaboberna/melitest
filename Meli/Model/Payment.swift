//
//  Payment.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/29/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class Payment: NSObject {
    var amount:String?
    var paymentMethodId:String?
    var issuerId:String?
    var installmentsMessage:String?
}
