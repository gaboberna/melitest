//
//  PaymentMethod.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/30/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit
import Gloss

class PaymentMethod: Glossy {
    var paymentMethodId: String!
    var imageName: String!
    var name: String!
    
    required public init?(json: JSON) {
        paymentMethodId     = "id"                  <~~ json
        imageName           = "secure_thumbnail"    <~~ json
        name                = "name"                <~~ json
    }
    
    func toJSON() -> JSON? {
        return nil
    }
}
