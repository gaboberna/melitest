//
//  StoreManager.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/29/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit
import Alamofire
import Gloss

class StoreManager: NSObject {
    let baseUrl = "https://api.mercadopago.com/v1/payment_methods"
    let publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88"

    public typealias completionBlockWithString = ((String) -> Void)?
    public typealias completionBlockWithPaymentMethodArray = ((Array<PaymentMethod>) -> Void)?
    public typealias completionBlockWithBankArray = ((Array<Bank>) -> Void)?
    public typealias completionBlockWithInstallmentArray = ((Array<Installment>) -> Void)?

    func getPaymentMethod(_ completionBlockSuccess:completionBlockWithPaymentMethodArray, completionBlockFail:completionBlockWithString){

        let params : Parameters = ["public_key":publicKey]
       
        Alamofire.request(baseUrl, method: .get, parameters:params , encoding: JSONEncoding(),headers:nil).responseJSON { (response) in
            
            Loader.sharedInstance.stopLoading()
            
            if (self.isInvalidResponse(response)) {
                completionBlockFail!((response.result.value as! NSDictionary).value(forKey:"message") as! String)
                
                return
            }
            let paymentMethods = [PaymentMethod].from(jsonArray:response.result.value! as! [JSON])

            completionBlockSuccess!(paymentMethods!)

        }
    }
    
    func getBankFromCreditCard(paymentMethodChoosen:String, completionBlockSuccess:completionBlockWithBankArray, completionBlockFail:completionBlockWithString) {
       
        let url = "card_issuers"
        let params : Parameters = ["public_key":publicKey,"payment_method_id":paymentMethodChoosen]
        
        Alamofire.request("\(baseUrl)/\(url)", method: .get, parameters:params , encoding: JSONEncoding(),headers:nil).responseJSON { (response) in
          
            Loader.sharedInstance.stopLoading()
            
            if (self.isInvalidResponse(response)) {
                completionBlockFail!((response.result.value as! NSDictionary).value(forKey:"message") as! String)
                
                return
            }
            
            let banks = [Bank].from(jsonArray:response.result.value as! Array<JSON>)
            
            completionBlockSuccess!(banks!)

        }
    }
    
    func getInstallments(paymentMethodChoosen:String,amount: String,issuerId:String,completionBlockSuccess:completionBlockWithInstallmentArray, completionBlockFail:completionBlockWithString) {

        if (!Utils.hasInternetConnection()) {
            
            Loader.sharedInstance.stopLoading()
            
            completionBlockFail!("")
            
            return
        }
        
        let url = "installments"
        let params : Parameters = ["public_key":publicKey,"amount":amount,"payment_method_id":paymentMethodChoosen,"issuer.id":issuerId]
        
        Alamofire.request("\(baseUrl)/\(url)", method: .get, parameters:params , encoding: JSONEncoding(),headers:nil).responseJSON { (response) in
            
            Loader.sharedInstance.stopLoading()
            
            if (self.isInvalidResponse(response)) {
                completionBlockFail!((response.result.value as! NSDictionary).value(forKey:"message") as! String)
                
                return
            }
            
            if let installmentsArray = ((response.result.value as! NSArray)[0] as! NSDictionary) ["payer_costs"]{
                let installments = [Installment].from(jsonArray:installmentsArray as! Array<JSON>)
                
                completionBlockSuccess!(installments!)

            }
        }
    }
    
    func isInvalidResponse(_ response:(DataResponse<Any>)) -> Bool {

        let result = response.result.value
       
        if (result is NSDictionary) {
             return (result as! NSDictionary).value(forKey: "error") != nil
        }
        
        return false
    }
    
}



