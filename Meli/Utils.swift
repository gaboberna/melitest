//
//  Utils.swift
//  Meli
//
//  Created by Gabriel Bernardini on 11/1/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit
import SystemConfiguration

class Utils: NSObject {
    typealias completionBlockEmpty = ((Void) -> Void)?

    class func hasInternetConnection() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    class func addAlertViewController(title:String, message:String, cancelButtonText:String, primaryButtonText:String?, completionBlock:completionBlockEmpty?){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let optionText = primaryButtonText {
            alertController.addAction(UIAlertAction(title: optionText, style: .default, handler: { (UIAlertAction) in
                if let _completionBlock = completionBlock {
                    _completionBlock!()
                }
            }))

        }
        
        alertController.addAction(UIAlertAction(title: cancelButtonText, style: .cancel, handler: nil))
        
        (UIApplication.shared.delegate as! AppDelegate).showAlertGlobally(alertController)

    }
}
