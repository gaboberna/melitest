//
//  BankTableViewController.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/30/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class BankTableViewController: UITableViewController {

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var bankOptionsArray = Array<Bank>()
    var payment = Payment()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "CreditCardTableViewCell", bundle: nil), forCellReuseIdentifier: "reuseIdentifier")
        
        self.callBanksService()
    }
    
    func callBanksService() {
        if !Utils.hasInternetConnection(){
            Utils.addAlertViewController(title: "Error", message: "No internet connection", cancelButtonText: "OK", primaryButtonText: "Retry", completionBlock: { () in
                self.callBanksService()
            })
            
            return
        }
        
        Loader.sharedInstance.startLoading(view:self.view)

        let storeManager = StoreManager()
        
        storeManager.getBankFromCreditCard(paymentMethodChoosen: payment.paymentMethodId!, completionBlockSuccess: { (banksArray) in
            self.bankOptionsArray = banksArray
            
            self.tableView.reloadData()
        }) { (error) in
            Utils.addAlertViewController(title: "Error", message: error, cancelButtonText: "OK", primaryButtonText: nil, completionBlock: nil)
        }
    }

    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankOptionsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? CreditCardTableViewCell
    
        if (cell == nil) {
        cell = CreditCardTableViewCell(style: .default, reuseIdentifier: "reuseIdentifier")
        }

        cell!.creditImageView.downloadImage(url: URL(string: bankOptionsArray[indexPath.row].imageName)!)
        cell!.creditCardLabel.text = bankOptionsArray[indexPath.row].name
    
        return cell!
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        payment.issuerId = bankOptionsArray[indexPath.row].bankId
        self.performSegue(withIdentifier: "installment", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let installmentsTableViewController = segue.destination as? InstallmentsTableViewController {
            installmentsTableViewController.payment = self.payment
        }
    }
}
