//
//  CreditCardsTableViewController.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/29/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class CreditCardsTableViewController: UITableViewController {

    var creditCardsArray = Array<PaymentMethod>()
    var payment: Payment!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "CreditCardTableViewCell", bundle: nil), forCellReuseIdentifier: "reuseIdentifier")

        self.callCreditCardsService()
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func callCreditCardsService() {
        if !Utils.hasInternetConnection(){
            Utils.addAlertViewController(title: "Error", message: "No internet connection", cancelButtonText: "OK", primaryButtonText: "Retry", completionBlock: { () in
                    self.callCreditCardsService()
            })
            
            return
        }
        
        
        Loader.sharedInstance.startLoading(view:self.view)
        
        let storeManager = StoreManager()
        storeManager.getPaymentMethod({ (paymentMethodsArray) in
            self.creditCardsArray = paymentMethodsArray
            self.tableView.reloadData()
        }) { (error) in
            Utils.addAlertViewController(title: "Error", message: error, cancelButtonText: "OK", primaryButtonText: nil, completionBlock: nil)
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return creditCardsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? CreditCardTableViewCell

        if (cell == nil) {
            cell = CreditCardTableViewCell(style: .default, reuseIdentifier: "reuseIdentifier")
        }
        
        cell!.creditImageView.downloadImage(url: URL(string: creditCardsArray[indexPath.row].imageName)!)
        cell!.creditCardLabel.text = creditCardsArray[indexPath.row].name

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        payment.paymentMethodId = creditCardsArray[indexPath.row].paymentMethodId
        self.performSegue(withIdentifier: "bank", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let bankTableViewController = segue.destination as? BankTableViewController {
            bankTableViewController.payment = payment
        }
    }

}
