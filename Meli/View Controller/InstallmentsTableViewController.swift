//
//  InstallmentsTableViewController.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/31/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class InstallmentsTableViewController: UITableViewController {
    
    var installmentsArray = Array<Installment>()
    var payment = Payment()
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "reuseIdentifier")
        
        self.callInstallmentsService()
    }
    
    func callInstallmentsService() {
        if !Utils.hasInternetConnection(){
            Utils.addAlertViewController(title: "Error", message: "Sin conexión a internet", cancelButtonText: "OK", primaryButtonText: "Reintentar", completionBlock: { () in
                self.callInstallmentsService()
            })
            
            return
        }
    
        Loader.sharedInstance.startLoading(view:self.view)

        let storeManager = StoreManager()

        storeManager.getInstallments(paymentMethodChoosen: payment.paymentMethodId!, amount: payment.amount!, issuerId: payment.issuerId!, completionBlockSuccess: { (installsments) in
            self.installmentsArray = installsments
            
            self.tableView.reloadData()
        }) { (error) in
            Utils.addAlertViewController(title: "Error", message: error, cancelButtonText: "OK", primaryButtonText: nil, completionBlock: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return installmentsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        if (cell == nil) {
            cell = UITableViewCell(style: .default, reuseIdentifier: "reuseIdentifier")
        }
        
        cell.textLabel?.text = installmentsArray[indexPath.row].message
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        payment.installmentsMessage = installmentsArray[indexPath.row].message
        
        self.backToRoot()
    }
    
    func backToRoot() {
        let storyBrd = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyBrd.instantiateInitialViewController() as! ViewController
        controller.payment = self.payment
        self.view.window?.rootViewController = controller
        
        storyBrd.instantiateViewController(withIdentifier:"root")
        
        controller.showPayment()
    }

}
