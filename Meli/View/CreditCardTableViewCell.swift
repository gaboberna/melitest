//
//  CreditCardTableViewCell.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/29/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class CreditCardTableViewCell: UITableViewCell {

    @IBOutlet weak var creditImageView: UIImageView!
    @IBOutlet weak var creditCardLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()        
    }
    
}
