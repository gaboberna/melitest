//
//  ViewController.swift
//  Meli
//
//  Created by Gabriel Bernardini on 10/28/17.
//  Copyright © 2017 Gabriel Bernardini. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate {
    var payment = Payment()
    @IBOutlet weak var amountTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupView () {
        self.view.isUserInteractionEnabled = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ViewController.hideKeyboard))
        
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func hideKeyboard() {
        amountTextField.resignFirstResponder()
    }
    
    func isValidAmount() -> Bool {
        return amountTextField.text!.characters.count > 0
    }
    
    @IBAction func continuePressed(_ sender: Any) {
        if (isValidAmount()){
            payment.amount = amountTextField.text!
            self.performSegue(withIdentifier: "paymentMethod", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let creditCardsTableViewController = segue.destination as? CreditCardsTableViewController {
            creditCardsTableViewController.payment = payment
        }
    }
    
    func showPayment(){
        
        Utils.addAlertViewController(title: "Pago", message: "monto: \(payment.amount!) \nmétodo de pago: \(payment.paymentMethodId!) \nbanco: \(payment.issuerId!) \ndetalle de cuotas: \(payment.installmentsMessage!)", cancelButtonText: "OK", primaryButtonText: nil, completionBlock: nil)
        
    }
    
    
}

